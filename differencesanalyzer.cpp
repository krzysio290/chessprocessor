#include "differencesanalyzer.h"

differencesAnalyzer::differencesAnalyzer()
{
}

std::string differencesAnalyzer::moveToString(int x, int y) {
    std::string move = "";
    move += (char)((int)'A'+x);
    move += (char)((int)'1'+(7-y));
    return move;
}

std::string differencesAnalyzer::logMove(std::vector<std::vector<int> > chessTable1, std::vector<std::vector<int> > chessTable2) {
    int changeCounter = 0;
    std::string start = "";
    std::string end = "";
    int tempX = -1, tempY = -1; //jedynie na wypadek en passant
    for(int y = 0; y<8; y++) {
        for(int x = 0; x<8; x++) {
            if(chessTable1[x][y]!=chessTable2[x][y]) {
                changeCounter++;
                if(chessTable2[x][y]==0) {
                    //cout << "Stad sie ruszono: (" << x << "," << y << ")" << endl;
                    start += moveToString(x,y);
                }
                if(chessTable1[x][y]==1 && chessTable2[x][y]==2 || chessTable1[x][y]==2 && chessTable2[x][y]==1) {
                    //cout << "Bicie na: (" << x << "," << y << ")" << endl;
                    end += moveToString(x,y);
                }
                if(chessTable1[x][y]==0 && chessTable2[x][y]==2 || chessTable1[x][y]==0 && chessTable2[x][y]==1) {
                    //cout << "Ruch na: (" << x << "," << y << ")" << endl;
                    end += moveToString(x,y);
                    tempX = x;
                    tempY = y;
                }
            }
        }
    }
    if(changeCounter>2) {
        //cout << "Nonstandard move - roszada krotka/dluga/lotny"; //zaimplementowac obsluge niestandardowych ruchow
        //Dluga roszada na gorze
        if( (chessTable1[0][0]==1 || chessTable1[0][0]==2) && chessTable1[0][0]==0) {
            return "O-O-O";
        }
        //Dluga roszada na dole
        if( (chessTable1[0][7]==1 || chessTable1[0][7]==2) && chessTable1[0][7]==0) {
            return "O-O-O";
        }
        //Krotka roszada na gorze
        if( (chessTable1[7][0]==1 || chessTable1[7][0]==2) && chessTable1[7][0]==0) {
            return "O-O";
        }
        //Dluga roszada na gorze
        if( (chessTable1[7][7]==1 || chessTable1[7][7]==2) && chessTable1[7][7]==0) {
            return "O-O";
        }
        //Bicie w przelocie - na pewno gdy zadne z powyzszych
//        if(tempY==5 && ( (chessTable1[(tempX-1>=0?tempX-1:tempX+1)][tempY-1]!=0 && chessTable2[(tempX-1>=0?tempX-1:tempX+1)][tempY-1]==0) || (chessTable1[(tempX-1>=0?tempX-1:tempX+1)][tempY-1]!=0 && chessTable2[tempX+1][tempY-1]==0)) ) {
//            return ""+moveToString(tempX,tempY-1)+":"+end+"(e.p.)"; //en passant
//        }
        return "(e.p.)"; //en passant
    }
    return start+":"+end;
}
