#ifndef CHESSPROCESSOR_H
#define CHESSPROCESSOR_H
#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;

struct Point {
   Point(int x, int y) {
       this->x = x;
       this->y = y;
   }
   int x;
   int y;
   int player;
   bool operator==(Point &point) {
       return (this->x == point.x && this->y == point.y ? true : false);
   }
   bool operator!=(Point &point) {
        return !(this->operator==(point));
   }
};

class ChessProcessor {
public:
    enum PointPosition {TopLeft, TopRight, BottomLeft, BottomRight };
    ChessProcessor();
    vector<Point> selectCorners(vector<Point> &vec);
    PointPosition getCornerPosition(vector<Point> corners, Point corner);
    Point getCornerOnPosition(vector<Point> corners, PointPosition position);
    int getMatrixRow(Point point, vector<Point> corners);
    int getMatrixColumn(Point point, vector<Point> corners);
private:
    double getRatioA(Point xy1, Point xy2);
    double getRatioB(double ratioA, Point xy);
    double getLinearFunctionValue(double ratioA, double ratioB, int x);
    double getAverageRatioA(Point firstPointLine1, Point secondPointLine1, Point firstPointLine2, Point secondPointLine2);
    double getLineXIntersectionThroughPoint(double averageRatioA, Point corner1, Point corner2, Point point);
    double getLineYIntersectionThroughPoint(double averageRatioA, Point corner1, Point corner2, Point point);
    int    getColumn(Point bottomPoint1, Point bottomPoint2, double intersectionX);
    int    getRow(Point bottomPoint1, Point bottomPoint2, double intersectionY);
    int comparePointToLine(const Point &linePoint1, const Point &linePoint2, const Point &pointToCheck);
};

void printPoint(const Point &point);
#endif // CHESSPROCESSOR_H
