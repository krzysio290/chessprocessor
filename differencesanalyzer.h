#ifndef DIFFERENCESANALYZER_H
#define DIFFERENCESANALYZER_H
#include <string>
#include <vector>
//using namespace std;
class differencesAnalyzer
{
public:
    differencesAnalyzer();
    static std::string moveToString(int x, int y);
    static std::string logMove(std::vector<std::vector<int> > chessTable1, std::vector<std::vector<int> > chessTable2);
};

#endif // DIFFERENCESANALYZER_H
