#include <iostream>
#include <vector>
#include <algorithm>
#include "chessprocessor.h"
#include "differencesanalyzer.h"
#define LOG

using namespace std;

string logMove(vector<vector<int> > chessTable1, vector<vector<int> > chessTable2);

int main(int argc, char* argv[]) {
    vector<Point> points;
    vector<Point> corners;

    points.push_back(Point(62,135)); //MARKER
    points.push_back(Point(108,177));
    points.push_back(Point(190,173));
    points.push_back(Point(277,177));
    points.push_back(Point(357,182));
    points.push_back(Point(439,187));
    points.push_back(Point(507,183));
    points.push_back(Point(584,184));
    points.push_back(Point(644,185));
    points.push_back(Point(698,132)); //MARKER - Y nie jest idealnie spisany
    //drugi rzad
    points.push_back(Point(112,261));
    points.push_back(Point(194,265));
    points.push_back(Point(274,269));
    points.push_back(Point(356,267));
    points.push_back(Point(436,269));
    points.push_back(Point(507,266));
    points.push_back(Point(584,265));
    points.push_back(Point(663,268));
    //trzeci rzad
    points.push_back(Point(108,675));
    points.push_back(Point(193,673));
    points.push_back(Point(277,667));
    points.push_back(Point(359,669));
    points.push_back(Point(440,662));
    points.push_back(Point(514,660));
    points.push_back(Point(597,658));
    points.push_back(Point(667,653));
    //czwarty rzad
    points.push_back(Point(110,749));
    points.push_back(Point(193,754));
    points.push_back(Point(282,749));
    points.push_back(Point(354,748));
    points.push_back(Point(446,741));
    points.push_back(Point(515,736));
    points.push_back(Point(597,733));
    points.push_back(Point(672,736));
    points.push_back(Point(66,799));  //MARKER
    points.push_back(Point(708,789)); //MARKER
    ChessProcessor processor;

    vector<Point> narozniki = processor.selectCorners(points);

    //Point punkt(507,183);
    Point punkt(110,749);
    cout << "Kolumna jest w: " << processor.getMatrixColumn(punkt, narozniki) << endl;
    cout << "Wiersz jest w : " << processor.getMatrixRow(punkt, narozniki) << endl;

    vector<vector<int> > chessTable(8,vector<int>(8));
    for(int y = 0; y<8; y++) { //Zerowanie macierzy
        for(int x = 0; x<8; x++) {
            chessTable[x][y] = 0;
        }
    }

    for(vector<Point>::iterator it = points.begin(); it!=points.end(); it++) { //Przepisanie wartosci z bierek
        //chessTable[processor.getMatrixColumn(*it, narozniki)][processor.getMatrixRow(*it, narozniki)] = it->player;
        chessTable[processor.getMatrixColumn(*it, narozniki)][processor.getMatrixRow(*it, narozniki)] = 2; //Please correct to proper value
    }
    for(int y = 6; y<8; y++) {
        for(int x = 0; x<8; x++) {
            chessTable[x][y] = 1;
        }
    }
    chessTable[4][1] = 0;
    chessTable[5][6] = 0;
    chessTable[4][4] = 2;
    chessTable[5][4] = 1;
    //-----Druga macierz
    vector<vector<int> > chessTable2(8,vector<int>(8));
    for(int y = 0; y<8; y++) //Zerowanie macierzy
        for(int x = 0; x<8; x++)
            chessTable2[x][y] = 0;
    for(int y = 0; y<2; y++) //Zerowanie macierzy
        for(int x = 0; x<8; x++)
            chessTable2[x][y] = 2;
    for(int y = 6; y<8; y++) //Zerowanie macierzy
        for(int x = 0; x<8; x++)
            chessTable2[x][y] = 1;
    chessTable2[5][6] = 0;
    chessTable2[4][1] = 0;
    chessTable2[4][4] = 0;
    //chessTable2[5][4] = 1;
    chessTable2[5][4] = 0;
    chessTable2[5][5] = 2;




    for(int y = 0; y<8; y++) { //Print
        for(int x = 0; x<8; x++) {
            cout << chessTable[x][y] << " ";
        }
        cout << endl;
    }
    cout << "-----------------" << endl;
    for(int y = 0; y<8; y++) { //Print
        for(int x = 0; x<8; x++) {
            cout << chessTable2[x][y] << " ";
        }
        cout << endl;
    }

    cout << differencesAnalyzer::logMove(chessTable, chessTable2) << endl;

    return 0;
}

