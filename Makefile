main: main.o chessprocessor.o differencesanalyzer.o
	g++ main.o chessprocessor.o differencesanalyzer.o -o main
main.o: main.cpp
	g++ -c main.cpp -o main.o
chessprocessor.o: chessprocessor.cpp
	g++ -c chessprocessor.cpp -o chessprocessor.o
differencesanalyzer.o: differencesanalyzer.cpp
	g++ -c differencesanalyzer.cpp -o differencesanalyzer.o
clean:
	rm -f main *.o
