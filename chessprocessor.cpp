#include "chessprocessor.h"
//#define LOG
//#define LOG_COUNTERS
ChessProcessor::ChessProcessor()
{
}

/* Name: printPoint
 * Description: Print point on console
 * Type: I don't know
 */
void printPoint(const Point &point) {
    cout << "(" << point.x << "," << point.y << ")" << endl;
}

/* Name: selectCorners
 * Description: The function find and return corners from all points
 * Type: Alghoritm
 */
vector<Point> ChessProcessor::selectCorners(vector<Point> &vec) {
    vector<Point> corners;
    //Find the highest point
    int yHighestPoint = 0;
    int positionHighest = 0;
    for(int i = 0; i<vec.size(); i++) {
        if(yHighestPoint<vec[i].y) {
            yHighestPoint = vec[i].y;
            positionHighest = i;
        }
    }
    corners.push_back(vec[positionHighest]);
    vec.erase(vec.begin()+positionHighest);

    int yLowestPoint = 9999999;
    int positionLowest = 0;
    for(int i = 0; i<vec.size(); i++) { //Find lowest point
        if(yLowestPoint>vec[i].y) {
            yLowestPoint = vec[i].y;
            positionLowest = i;
        }
    }
    corners.push_back(vec[positionLowest]);
    vec.erase(vec.begin()+positionLowest);
#ifdef LOG
    for_each(corners.begin(), corners.end(), printPoint);
    cout << "=================" << endl;
#endif
    //Get first corner and find next with the rest under the line
    vector<Point> found;
    int counterAbove = 0;
    int counterBelow = 0;
    for(vector<Point>::iterator corner = corners.begin(); corner!=corners.end(); corner++) {
        for(vector<Point>::iterator corner2 = vec.begin(); corner2!=vec.end(); corner2++) {
            if(*corner == *corner2)
                continue;
            for(vector<Point>::iterator testPoint = vec.begin(); testPoint!=vec.end(); testPoint++) {
                int position = comparePointToLine(*corner, *corner2, *testPoint);
                if(position>0) {
                    counterAbove++;
                } else if(position<0) {
                    counterBelow++;
                }
                if(*testPoint==vec[vec.size()]) {
                    if(counterAbove > 0 && counterBelow == 0) {
#ifdef LOG
                        cout << "Znaleziono punkt: ";
                        printPoint(*corner2);
#endif
                        found.push_back(*corner2);
                    } else if(counterBelow > 0 && counterAbove == 0) {
#ifdef LOG
                        cout << "Znaleziono punkt: ";
                        printPoint(*corner2);
#endif
                        found.push_back(*corner2);
                    }
                    counterAbove = 0;
                    counterBelow = 0;
                }
            }
        }
    }
    //=================== Remove redundant elements =======================
    for(vector<Point>::iterator it = found.begin(); it!=found.end(); it++) {
        for(vector<Point>::iterator it2 = found.begin(); it2!=found.end(); it2++) {
            if(*it==*it2 && it!=it2) { //Different elements with the same values
                found.erase(it);
                it = found.begin();
                it2 = found.begin();
                break;
            }
        }
    }
    for(int i = 0; i < found.size(); i++) {
        corners.push_back(found[i]);
    }
    found.clear();
    for(vector<Point>::iterator it = corners.begin(); it!=corners.end(); it++) {
        for(int i = 0; i < vec.size(); i++) {
            if(vec[i]==*it) {
                vec.erase(vec.begin()+i);
                break;
            }
        }
    }
#ifdef LOG
    for_each(corners.begin(), corners.end(), printPoint);
#endif
    return corners;
}

/* Name: comparePointToLine
 * Description: The function returns the position of a point relative to a simple
 * Type: Alghoritm
 */
int ChessProcessor::comparePointToLine(const Point &linePoint1, const Point &linePoint2, const Point &pointToCheck) {
    double ratioA = getRatioA(linePoint1,linePoint2);
    double wartosc = getLinearFunctionValue(ratioA,getRatioB(ratioA,linePoint1),pointToCheck.x);
    if(pointToCheck.y == wartosc) {
        return 0;
    } else if( !(pointToCheck.y > wartosc) ) { //NEGACJA, BO UKL. WSPOLRZEDNYCH JEST ODWROCONY
        return 1; //punkt powyzej
    } else {
        return -1;
    }
}

/* Name: getCornerPosition
 * Description: Function find place of corner
 * Type: Alghoritm
 */
ChessProcessor::PointPosition ChessProcessor::getCornerPosition(vector<Point> corners, Point corner) {
    int counterH = 0;
    int counterV = 0;
    for(vector<Point>::iterator it = corners.begin(); it!=corners.end(); it++) {
        if(corner!=*it) {
            if(corner.x > it->x) {
                counterV++; //Wyszukany naroznik jest bardziej w prawo
            } else {
                counterV--;
            }
            if(corner.y > it->y) {
                counterH++; //Naroznik jest nizej
            } else {
                counterH--;
            }
        }
    }
    if(counterH > 0 && counterV > 0) {
#ifdef LOG_COUNTERS
        cout << "Counters: " << counterH << " " << counterV << endl;
#endif
        return BottomRight;
    }
    if(counterH < 0 && counterV > 0) {
#ifdef LOG_COUNTERS
        cout << "Counters: " << counterH << " " << counterV << endl;
#endif
        return TopRight;
    }
    if(counterH > 0 && counterV < 0) {
#ifdef LOG_COUNTERS
        cout << "Counters: " << counterH << " " << counterV << endl;
#endif
        return BottomLeft;
    }
    if(counterH < 0 && counterV < 0) {
#ifdef LOG_COUNTERS
        cout << "Counters: " << counterH << " " << counterV << endl;
#endif
        return TopLeft;
    }
}

/* Name: getCornerOnPosition
 * Description: Function find needed corner - positions contains enum PointPosition
 * Type: Alghoritm
 */
Point ChessProcessor::getCornerOnPosition(vector<Point> corners, ChessProcessor::PointPosition position) {
    vector<Point>::iterator corner = corners.begin();
    Point foundPoint(0,0);
    bool found = false;
    do {
        if(getCornerPosition(corners, *corner) == position) {
            found = true;
            foundPoint = *corner;
        }
        ++corner;
    } while(!found && corner!=corners.end());
    return foundPoint;
}

/* Name: getAverageRatioA
 * Description: Function calculating average ratio A using 4 points
 * Equation: averageRatio=(ratioOfLine1+ratioOfLine2)/2;
 */
double ChessProcessor::getAverageRatioA(Point firstPointLine1, Point secondPointLine1, Point firstPointLine2, Point secondPointLine2) {
    double firstLineRatioA = getRatioA(firstPointLine1, secondPointLine1);
    double secondLineRatioA = getRatioA(firstPointLine2, secondPointLine2);
    return (firstLineRatioA+secondLineRatioA)/2;
}

/* Name: getLineXIntersectionThroughPoint
 * Description: Function calculating x of point common to line and line through selected point
 * Equation: x = (b1-b2)/(a2-a1);
 */
double ChessProcessor::getLineXIntersectionThroughPoint(double averageRatioA, Point corner1, Point corner2, Point point) {
    double lineRatioA = getRatioA(corner1, corner2);
    double calculatedRatio = (getRatioB(lineRatioA,corner1)-getRatioB(averageRatioA,point))/(lineRatioA-averageRatioA);
    return fabs(calculatedRatio);
}

/* Name: getLineYIntersectionThroughPoint
 * Description: Function calculating y of point common to line and line through selected point
 * Equation: y = (a2*b1-b2*a1)/(a2-a1);
 */
double ChessProcessor::getLineYIntersectionThroughPoint(double averageRatioA, Point corner1, Point corner2, Point point) {
    double lineRatioA = getRatioA(corner1, corner2); //a2
    double lineRatioB = getRatioB(averageRatioA, point); //b1
    double line2RatioB = getRatioA(corner1,corner2);
    double calculatedRatio = (lineRatioA*lineRatioB - line2RatioB*averageRatioA)/(lineRatioA-averageRatioA);
    return fabs(calculatedRatio);
}

/* Name: getColumn
 * Description: Function calculating column on chess table (0 - LEFT, 7 - RIGHT)
 * Type: Alghoritm
 */
int ChessProcessor::getColumn(Point bottomPoint1, Point bottomPoint2, double intersectionX) {
    int column;
    int startX;
    if(bottomPoint1.x < bottomPoint2.x) {
        startX = bottomPoint1.x;
    } else {
        startX = bottomPoint2.x;
    }
    double step = fabs(bottomPoint2.x-bottomPoint1.x)/8;
    for(column = 0; column < 8; column++) {
        if(intersectionX < startX+(column+1)*step) {
            break;
        }
    }
    return column;
}

/* Name: getRow
 * Description: Function calculating row on chess table (0 - UP, 7 - BOTTOM)
 * Type: Alghoritm
 */
int ChessProcessor::getRow(Point bottomPoint1, Point bottomPoint2, double intersectionY) {
    int row;
    int startY;
    if(bottomPoint1.y < bottomPoint2.y) {
        startY = bottomPoint1.y;
    } else {
        startY = bottomPoint2.y;
    }
    double step = fabs(bottomPoint2.y-bottomPoint1.y)/8;
    for(row = 0; row < 8; row++) {
        if(intersectionY < startY+(row+1)*step) {
            break;
        }
    }
    return row;
}

/* Name: getRatioA
 * Description: Function calculating ratio A for line crossing two points
 * Type: ratioA = (y2-y1)/(x2-x1);
 */
double ChessProcessor::getRatioA(Point xy1, Point xy2) {
    if(xy2.x-xy1.x == 0) {
#ifdef LOG
        cout << "Division by zero!" << endl;
#endif
        return 9999.0;
    } else {
        return (double)(xy2.y-xy1.y)/(double)(xy2.x-xy1.x);
    }
}

/* Name: getRatioB
 * Description: Function calculating ratio B for line with ratio A and crossing point
 * Type: ratioB = y-ratioA*x;
 */
double ChessProcessor::getRatioB(double ratioA, Point xy) {
    return (double)xy.y-ratioA*xy.x;
}

/* Name: getLinearFunctionValue
 * Description: Function calculating value in x for line
 * Type: value = ratioA*x+ratioB;
 */
double ChessProcessor::getLinearFunctionValue(double ratioA, double ratioB, int x) {
    return ratioA*x+ratioB;
}

/* Name: getMatrixRow
 * Description: High level interface - function calculating row on table A-H
 * Type: Alghoritm
 */
int ChessProcessor::getMatrixRow(Point point, vector<Point> corners) {
    double averageRatio = getAverageRatioA(getCornerOnPosition(corners,ChessProcessor::TopLeft), getCornerOnPosition(corners,ChessProcessor::TopRight), getCornerOnPosition(corners,ChessProcessor::BottomLeft),getCornerOnPosition(corners,ChessProcessor::BottomRight));
    double intersectionY = getLineYIntersectionThroughPoint(averageRatio,getCornerOnPosition(corners,ChessProcessor::TopLeft),getCornerOnPosition(corners,ChessProcessor::BottomLeft),point);
    return getRow(getCornerOnPosition(corners,ChessProcessor::TopLeft),getCornerOnPosition(corners,ChessProcessor::BottomLeft), intersectionY);
}

/* Name: getMatrixColumn
 * Description: High level interface - function calculating column on table 1-8
 * Type: Alghoritm
 */
int ChessProcessor::getMatrixColumn(Point point, vector<Point> corners) {
    double averageRatio = getAverageRatioA(getCornerOnPosition(corners,ChessProcessor::TopLeft), getCornerOnPosition(corners,ChessProcessor::BottomLeft), getCornerOnPosition(corners,ChessProcessor::TopRight),getCornerOnPosition(corners,ChessProcessor::BottomRight));
    double intersectionX = getLineXIntersectionThroughPoint(averageRatio,getCornerOnPosition(corners,ChessProcessor::BottomLeft),getCornerOnPosition(corners,ChessProcessor::BottomRight),point);
    return getColumn(getCornerOnPosition(corners,ChessProcessor::BottomLeft),getCornerOnPosition(corners,ChessProcessor::BottomRight), intersectionX);
}
